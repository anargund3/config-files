set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'ervandew/supertab'
" Plugin 'lsdr/monokai'
Plugin 'davidhalter/jedi-vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'sjl/badwolf'
Plugin 'vim-syntastic/syntastic'
Plugin 'Rip-Rip/clang_complete'
Plugin 'vim-scripts/Pydiction'
"Plugin 'git://github.com/jiangmiao/auto-pairs.git'
" Plugin 'Valloric/YouCompleteMe'
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
"filetype plugin indent on    " required

set number
set cursorline
set autoindent
set tabstop=4
set expandtab
set tw=80
set fo+=t
set wrap linebreak nolist

"colorscheme badwolf 
colorscheme monokai

let &colorcolumn=join(range(81,999),",")
let &colorcolumn="80,".join(range(400,999),",")

let g:jedi#popup_on_dot = 0
let g:jedi#show_call_signatures = 0
let g:jedi#documentation_command = 0
let g:pymode_rope = 0
let g:indent_guides_guide_size = 1
let g:indent_guides_color_change_percent = 0
let g:indent_guides_enable_on_vim_startup = 1
"let mapleader=" "
"map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>
"let g:ycm_python_binary_path = 'python'
let g:pydiction_location = '/home/avinash/.vim/bundle/Pydiction/complete-dict' 
let g:pydiction_menu_height = 3
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_quiet_messages = { "type": "style" }

let g:clang_library_path='/usr/lib/x86_64-linux-gnu/libclang-3.6.so.1'

let python_highlight_all=1
syntax on
filetype plugin on
set omnifunc=syntaxcomplete
set backspace=indent,eol,start
